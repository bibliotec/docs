# Bibliotec documentation

Bibliotec is a self-hosted search engine for local libraries.

For general usage information, see the [project wiki](https://gitlab.com/bibliotec/docs/-/wikis/Home).

## Features
- File exploration
- Full-text search
- RESTful API
- OAuth2 authentication

## Important links

- [Server repository](https://gitlab.com/bibliotec/server)
- [Web client repository](https://gitlab.com/bibliotec/web-client)

## Development

- [Git](https://git-scm.com/)
- [NodeJS](https://nodejs.org/)
- [Python](https://python.org)
- [Docker](https://docker.com)

## Installation

Create a directory dedicated to Bibliotec and clone the server and/or the web client:

```
cd /path/to/your/projects/home
mkdir bibliotec
cd bibliotec
```

```
git clone https://gitlab.com/bibliotec/server.git
```

```
git clone https://gitlab.com/bibliotec/web-client.git

```

To develop the web client, install the Node.js dependencies:

```
cd web-client
npm i
```


To develop Bibliotec's server, create a [virtual environment](https://docs.python.org/3/tutorial/venv.html) and install all dependencies:

```
cd ../server
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
```

Then run the database migrations:

```
python manage.py migrate
```

## Usage

With everything installed, it's time launch the back-end using [Docker Compose](https://docs.docker.com/compose/). This will run the Django project and initiate the Solr instance:

```
docker-compose up
```

Now, run the client application:

```
npm run dev
```

Then, head to http://localhost:5000 on your web browser.
